Feature: Performing a series of tests

	As a user i should be able to use all features.

	Background:
		Given I am on the home page

	Scenario Outline: Select option on dropdown
	  When I click on 'Dropdown' link
	  And I select "<option>" on dropdown
	  Then The correct option will be selected
	  # Then I test

	  Examples:
	  | option   |
	  | Option 1 |
	  | Option 2 |


	Scenario: User corrects Log In information and logs in
	  When I click on 'Form Authentication' link
	  And I fill username field
	  And I fill password field
	  And I append information on username field
	  And I click on login button
	  Then Error message will be displayed

	Scenario: User logs in
	  When I click on 'Form Authentication' link
	  And I fill correct username field
	  And I fill correct password field
	  And I click on login button
	  Then User logs in

	Scenario: Select checkbox
	  When I click on 'Checkboxes' link
	  And I select the 'checkbox 1' Checkbox
	  Then The 'checkbox 1' Checkbox will be selected

	Scenario: Deselect checkbox
	  When I click on 'Checkboxes' link
	  And I deselect the 'checkbox 2' Checkbox
	  Then The 'checkbox 2' Checkbox will be deselected

	Scenario: Take screenshot of avatar and text of each post
		When I click on 'Dynamic Content' link
		And I show on the console the link to the avatar and text of each post
		And I take a screenshot
		And I refresh the page
		And I show on the console the link to the avatar and text of each post
		And I take a screenshot   
		Then The information changed after refreshing the page
		And The screenshots should have been taken

	Scenario: Download and show contents of file
	  When I click on 'File Download' link
	  And I download a txt file
	  And I print its contents in the console
	  Then The txt file will be downloaded and the contents of the txt file will be displayed on console

	# Scenario: Drag boxes
	#   When I click on 'Drag and Drop' link
	#   And I drag A box into B box
	#   And I drag B box into A box
	#   Then The boxes will be switched two times
