import Page from './page'

class CheckboxesPage {

  get pageTitle ()   { return browser.element('h3') }
  get firstCheckbox ()     { return browser.element('input:first-child') }
  get lastCheckbox ()     { return browser.element('input:last-child') }


  clickCheckbox(num) {
  	if ( num === '1' ) {
  		this.firstCheckbox.waitForVisible()
  		this.firstCheckbox.click()
  	} else if ( num === '2' ) {
  		this.lastCheckbox.waitForVisible()
  		this.lastCheckbox.click()
  	}
  }

  checkCheckboxes(num) {
  	if (num === '1') {
		return this.firstCheckbox.isSelected()
  	}else if (num === '2') {
  		return this.lastCheckbox.isSelected()
  	}
  }

}

export default new CheckboxesPage()