import Page from './page'
// import DragAndDropHelper from './drag_and_drop_helper'

class DragAndDropPage {

  get pageTitle ()   	{ return browser.element('h3') }
  get BoxA () 		  	{ return browser.elements('div#column-a') }
  get BoxB () 		  	{ return browser.elements('div#column-b') }

    dragAndDropAB() {

		this.BoxA.waitForVisible()
		this.BoxB.waitForVisible()

    browser.pause(1000)

    browser.dragAndDrop(this.BoxA.selector, this.BoxB.selector)

    browser.pause(2000)

   	}

}

export default new DragAndDropPage()