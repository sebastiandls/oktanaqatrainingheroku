import Page from './page'
import { World } from '../World';

World.optionSelectedText
World.optionSelectedValue

class DropdownPage {

  get pageTitle ()   { return browser.element('h3') }
  get options ()     { return browser.elements('option') }
  get select ()     { return browser.elements('select#dropdown') }

  selectOption(option) {
    for (let i of this.options.value) {
      if ( i.getText() === option ) {
        World.optionSelectedText = option 
        World.optionSelectedValue = i.getValue()         
        i.click()
      }
    }
  }
 
  checkOptionSelected() {
    //agarro el value (num de posicion en options) y el texto del seleccionado
    //comparo en el string split de select, que split[valueSeleccionado] === texto del seleccionado
    //No se que tanto realmente comprueba esto

    var split = this.select.getText().split("\n");
    if ( split[World.optionSelectedValue].includes(World.optionSelectedText) ) {
      return true
    }else {
      return false
    }
  }

}

export default new DropdownPage()
