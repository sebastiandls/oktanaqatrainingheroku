import Page from './page'
import { World } from '../World';

World.log = ['', '']
let i = 0
class DynamicContentPage {

	get pageTitle ()  	{ return browser.element('h3') }
	get image ()     	{ return browser.element('div#content div#content img') }
	get texts ()     	{ return browser.element('div#content div#content div.large-10') }
	get row ()     		{ return browser.elements('div#content div#content div.row') }


	logInformation() {
		World.log[i] += 'New window: \n'
		for (var card of this.row.value) {
			World.log[i] += 'New Row: \n'
			let img = browser.elementIdElement(card.value.ELEMENT, this.image.selector).getAttribute('src')
			World.log[i] += 'Image Source: ' + img
			let txt = browser.elementIdElement(card.value.ELEMENT, this.texts.selector).getText()
			World.log[i] += 'Text: ' + txt + '\n \n'
		}
		console.log(World.log[i])
		i++
	}

	checkInfoDifference() {
		return World.log[0] !== World.log[1]
	}

	takeScreenshot() {
		 browser.saveScreenshot('../screenshot' + i)
	}
	
	checkScreenshot() {
		var fs = require('fs');

		if (fs.existsSync('../screenshot')) {
			return true
		}else {
			return false
		}
	}

}

export default new DynamicContentPage()