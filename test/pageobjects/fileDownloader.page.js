import Page from './page'
let read = false
class FileDownloaderPage {

	get pageTitle ()   	{ return browser.element('h3') }
	get link ()     	{ return browser.element('a[href="download/some-file.txt"]') }

	
	downloadTxt() {
	this.link.waitForVisible()
	this.link.click()
	}
	
	logText() {
    const fs = require('fs');
		browser.pause(1000) 
        try {
			let texto = '/Users/sebastiandelossantos/Downloads/some-file.txt'
     		let retext = fs.readFileSync(texto,'utf8')
    	    console.log(retext)
    	    read = true
        }catch (err){
        	read = false
        }
	}
``
	checkRead() {
		return read
	}
}

export default new FileDownloaderPage()