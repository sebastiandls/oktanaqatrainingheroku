import Page from './page'
import constants from '../constants.js'

class FormAuthenticationPage {

  get pageTitle ()      { return browser.element('h2') }
  get usernameField ()  { return browser.element('input#username') }
  get passwordField ()  { return browser.element('input#password') }
  get boton ()          { return browser.element('button') }
  get error ()          { return browser.element('div#flash.error') }
  get success ()        { return browser.element('div.flash.success') }

  fillUsername() {
    this.usernameField.waitForVisible()
    this.usernameField.setValue(this.random())
  }

  fillPassword() {
    this.passwordField.waitForVisible()
    this.passwordField.setValue(this.random())
  }

  fillCorrectUsername() {
    this.usernameField.waitForVisible()
    this.usernameField.setValue(constants.username)
  }

  fillCorrectPassword() {
    this.passwordField.waitForVisible()
    this.passwordField.setValue(constants.password)
  }

  appendUsername() {
    this.usernameField.waitForVisible()
    let now = this.usernameField.getValue()
    console.log(now)
    this.usernameField.setValue(now + '@email.com')
  }

  clickBtn() {
    this.boton.waitForVisible()
    this.boton.click()
  }

  checkError() {
    return this.error.waitForVisible()
  }

  checkSuccess() {
    return this.success.waitForVisible()
  }

  random() {
    return Math.random().toString(36).substring(4);
  }
}

export default new FormAuthenticationPage()
