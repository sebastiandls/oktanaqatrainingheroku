import { defineSupportCode } from 'cucumber'
import HomePage from '../pageobjects/home.page';
import CheckboxesPage from '../pageobjects/checkboxes.page';

defineSupportCode(function({ When, Then }) {
  When(/^I click on 'Checkboxes' link$/, function() {
    HomePage.clickLink('Checkboxes')
  })

  When(/^I select the 'checkbox 1' Checkbox$/, function() {
    CheckboxesPage.clickCheckbox('1')
  })

  Then(/^The 'checkbox 1' Checkbox will be selected$/, function() {
    expect(CheckboxesPage.checkCheckboxes('1')).to.be.true
  })

  When(/^I deselect the 'checkbox 2' Checkbox$/, function() {
    CheckboxesPage.clickCheckbox('2')
  })

  Then(/^The 'checkbox 2' Checkbox will be deselected$/, function() {
    expect(CheckboxesPage.checkCheckboxes('2')).to.be.false
  })
})
