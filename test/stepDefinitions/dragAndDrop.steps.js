import { defineSupportCode } from 'cucumber'
import HomePage from '../pageobjects/home.page';
import DragAndDropPage from '../pageobjects/dragAndDrop.page';

defineSupportCode(function({ When, Then }) {
  When(/^I click on 'Drag and Drop' link$/, function() {
    HomePage.clickLink('Drag and Drop')
  })

  When(/^I drag A box into B box$/, function() {
  	DragAndDropPage.dragAndDropAB()
  })

  When(/^I drag B box into A box$/, function() {
  	DragAndDropPage.dragAndDropAB()

  })

  Then(/^The boxes will be switched two times$/, function() {

  })

})
