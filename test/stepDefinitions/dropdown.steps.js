import { defineSupportCode } from 'cucumber'
import HomePage from '../pageobjects/home.page';
import DropdownPage from '../pageobjects/dropdown.page';

defineSupportCode(function({ When, Then }) {
  When(/^I click on 'Dropdown' link$/, function() {
  	HomePage.clickLink('Dropdown')
  })

  When(/^I select "([^"]*)" on dropdown$/, function(option) {
  	DropdownPage.selectOption(option)
  })

  Then(/^The correct option will be selected$/, function() {
  	expect(DropdownPage.checkOptionSelected()).to.be.true
  })

})
