import { defineSupportCode } from 'cucumber'
import HomePage from '../pageobjects/home.page';
import DynamicContentPage from '../pageobjects/dynamicContent.page';

defineSupportCode(function({ When, Then }) {
	When(/^I click on 'Dynamic Content' link$/, function() {
		HomePage.clickLink('Dynamic Content')
	})

	When(/^I show on the console the link to the avatar and text of each post$/, function() {
		DynamicContentPage.logInformation()
	})

	When(/^I take a screenshot$/, function() {
		DynamicContentPage.takeScreenshot()
	})

	When(/^I refresh the page$/, function() {
		browser.url('http://the-internet.herokuapp.com/dynamic_content') 
	})

	Then(/^The information changed after refreshing the page$/, function() {
		expect(DynamicContentPage.checkInfoDifference()).to.be.true
	})
	
	Then(/^The screenshots should have been taken$/, function() {
		expect(DynamicContentPage.checkScreenshot()).to.be.true
	})
})
