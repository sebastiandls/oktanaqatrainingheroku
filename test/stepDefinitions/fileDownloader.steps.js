import { defineSupportCode } from 'cucumber'
import HomePage from '../pageobjects/home.page';
import FileDownloaderPage from '../pageobjects/fileDownloader.page';

defineSupportCode(function({ When, Then }) {
  When(/^I click on 'File Download' link$/, function() {
     HomePage.clickLink('File Download')
  })

  When(/^I download a txt file$/, function() {
    FileDownloaderPage.downloadTxt()
  })

  When(/^I print its contents in the console$/, function() {
    FileDownloaderPage.logText()
  })

  Then(/^The txt file will be downloaded and the contents of the txt file will be displayed on console$/, function() {
    expect(FileDownloaderPage.checkRead()).to.be.true
  })

})
