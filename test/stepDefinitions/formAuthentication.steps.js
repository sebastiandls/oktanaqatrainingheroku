import { defineSupportCode } from 'cucumber'
import HomePage from '../pageobjects/home.page';
import FormAuthenticationPage from '../pageobjects/formAuthentication.page';

defineSupportCode(function({ When, Then }) {
  When(/^I click on 'Form Authentication' link$/, function() {
      HomePage.clickLink('Form Authentication')
  })

  When(/^I fill username field$/, function() {
      FormAuthenticationPage.fillUsername()
  })

  When(/^I fill password field$/, function() {
      FormAuthenticationPage.fillPassword()
  })

  When(/^I append information on username field$/, function() {
      FormAuthenticationPage.appendUsername()
  })

  When(/^I click on login button$/, function() {
      FormAuthenticationPage.clickBtn()
  })

  When(/^I fill correct username field$/, function() {
      FormAuthenticationPage.fillCorrectUsername()
  })

  When(/^I fill correct password field$/, function() {
      FormAuthenticationPage.fillCorrectPassword()
  })

  Then(/^Error message will be displayed$/, function() {
      expect(FormAuthenticationPage.checkError()).to.be.true
  })

  Then(/^User logs in$/, function() {
      expect(FormAuthenticationPage.checkSuccess()).to.be.true
  })
  
})
